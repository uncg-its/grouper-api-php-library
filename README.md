# Grouper API PHP Library

Contact: matt.libera@uncg.edu - Information Technology Services, UNC Greensboro

---

# Introduction

This package is a PHP library used to interact with the Grouper REST APIs through JSON.

> NOTE: currently, this package is tested and verified working against `v2_2_000` of the API. This version of the API will be used (it is hard-coded in as a constant to the `GrouperApi` object). Be sure your Grouper instance can handle this.

## Scope

This package is built not (yet) as a comprehensive interface with the Grouper API - it is not written to perform every conceivable API call to Grouper. Specifically, this package was built to suit the needs of UNC Greensboro's Grouper usage, and performs only the functions necessary for day-to-day operations of Grouper at UNCG.

Please fork and add methods to this as you see fit / as your needs dictate.

---

# Installation

Run `composer require 'uncgits/grouper-api-php-library'` to install the package.

---

# Structure and Description

## The Grouper API object vs. the Grouper API Interface object

This package contains two main objects that serve specific purposes in their interaction with the Grouper API.

### The `GrouperApi` class

The base API class, `GrouperApi`, is a class whose primary responsibility is making semantically correct calls to the API, and to pass back the results. This class:

- performs cursory validation of input data, to ensure it is in the proper format
- formats the JSON body
- sends the proper request for each endpoint
- parses the response for its components
- passes back the result in an array format

Each method in this object corresponds with (and is named exactly after) a command in the Grouper API (for instance, "WsRestGetMembersRequest").

#### Structure of return data

Each API call will be returned in an array format, structured like so:

```php
[
    "meta" => [
        "time_attempted" => 1528987500
        "endpoint" => "https://your-grouper-url.com"
        "command" => "WsRestGetMembersRequest"
        "body_parameters" => [
            "param1" => "value1",
            "param2" => "value2",
        ]
        "method" => "post"
    ],
    "response" => [
        "result" => "SUCCESS"
        "message" => ""
        "httpCode" => 200
        "httpReason" => "200"
    ]
    "body" => {} // the body of the response from the Grouper API
]
```

You can expect this array structure to be present always, and the `['response']['result']` parameter to contain either "SUCCESS" or "FAILURE". For each API call, you are handed back relevant information to help troubleshoot - including the endpoint contacted, the command name (according to Grouper API documentation), the body parameters that were passed, and the method used.

### The `GrouperApiInterface` class

This class was intended to be the primary "go-between" for developers and the API. While the job of the `GrouperApi` class is to technically interface directly with the API on an endpoint-specific basis, this class is task- or transaction-oriented, and handles the heavy lifting of both:

1. actually getting the data back to you in both a format you want, and
2. handling errors that occur along the way.

An example:

There is a method in the `GrouperApiInterface` class called `getAllGroupsForSubjects()`. This method knows that it needs to utilize the `getGroups()` method on the `GrouperApi` class to do the actual API calls, but also handles things like pagination, and parsing out the return data to be formatted into a useful array such as:

```php
[
    "johndoe1" => [
        "group1",
        "group2",
        "group3",
    ],
    "janedoe1" => [
        "group4",
        "group5"
    ]
]
```

---

# Usage

The `GrouperApiInterface` class should always (generally) be used instead of the `GrouperApi` class. If you need direct interaction with the API class, you have two options.

1. The `GrouperApiInterface` class contains an `$api` property that you can reference using `$myInstance->api` - for instance `$myInstance->api->hasMembers()`
2. The `GrouperApiInterface` class itself delegates any calls to undefined methods down to the `$api` object anyway, so you could simply call `$myInstance->hasMembers()` and it will be delegated using the `__call` magic method.

## Instantiation, Authentication, and basic usage

For vanilla PHP applications, the best way to handle using this class is simply to instantiate it. You will need to utilize the setters for `host`, `username`, `password`, `applicationName` on the class as well, so that the class knows how to talk to your Grouper instance.

*Note: at this time this package supports only HTTP Basic auth (username/password). It is recommended to use an account that has appropriate permissions to accomplish what you need to accomplish with your application.*

Here is an example:

```php
use Uncgits\GrouperApi\GrouperApiInterface as Grouper;

$myInstance = new Grouper();

// these methods are on the `GrouperApi` class, but will be passed down via __call()
$myInstance->setHost('my-grouper-url.com/');
$myInstance->setUsername('username');
$myInstance->setPassword('supertopsecret');
$myInstance->setApplicationName('My Great Grouper App');

$groups = $myInstance->getAllGroupsForSubjects(['12345','67890']);
```

For use in a framework, you may be able to make use of this in a more efficient manner. If you are using Laravel (5.6+), we have a Laravel wrapper for this API package (`uncgits/grouper-api-wrapper-laravel`) that will include many framework-specific ways of making this process more efficient.

## Chain methods

The `GrouperApiInterface` can also make use of fluent chained methods to allow the developer to modify the API call. Available methods are found in the `Chainable` Trait (for clarity's sake), which the `GrouperApiInterface` class implements. So, calls like this are possible:

```php
$myInstance->asUser('johndoe1')->withSubjectDetail()->withGroupDetail()->getAllGroupsForSubjects(['12345','67890']);
```

These chain methods will add appropriate parameters to the body of the API call automatically.

### Built-in uses of chain methods

Many of the exposed methods on this class already implement some of these chain methods (for instance, `searchSubject()` calls `$this->withSubjectDetail()->withSubjectAttributeNamesString()->getSubjectsLite()`). Be aware of this as you begin to explore adding chained methods on your own.

### Available chain methods

#### `asUser()`

Allows the operation / API calls to be performed as another user - utilizes the `actAsSubjectLookup` parameter in the API call.

*Requires a Grouper user ID*

#### `withGroupDetail()`

Provides group details back on the API call (if supported) - utilizes the `includeGroupDetail` parameter with value of `T`

#### `withSubjectDetail()`

Provides subject details back on the API call (if supported) - utilizes the `includeSubjectDetail` parameter with value of `T`

#### `withMemberFilter()`

Allows the operation to search only for a specific affiliation of Members. Accepted values are `all`, `immediate`, `nonimmediate`. Utilizes the `memberFilter` parameter of the API call.

*Requires a filter type string*

#### `withSubjectAttributeNames()`

Allows the operation to request additional subject attributes, using the `subjectAttributeNames` parameter in the API call. There are two possible ways to use this method:

1. supply an array of attribute name strings to the method at the time of calling (e.g. `$myInstance->withSubjectAttributeNames(['one', 'two', 'three'])->...`), or
2. if there is no argument supplied, the method will use the value assigned to the `subjectAttributeNames` property on the class. So, using the setter to set the value earlier in the code (e.g. `$myInstance->setSubjectAttributeNames(['one', 'two', 'three'])`), and then calling the chain method with no arguments (e.g. `$myInstance->withSubjectAttributeNames()->...`) will also work.

*Requires either an array of strings or previous setting of `subjectAttributeNames` with an array of strings*

#### `withSubjectAttributeNamesString()`

The same as `withSubjectAttributeNames()`, except it formats the API parameter differently - as a string instead of an array. This is used typically for Lite API calls where the string format is expected. You use it the same way as you use `withSubjectAttributeNames()`. Check Grouper API documentation if you are unsure which of these two methods to use.

#### `withPagination()`

A mostly-internally-used chain method to set the `pageSize` and `pageNumber` parameters in the API call, with the intent of using these parameters to page through several API calls to retrieve a dataset. Read below on Pagination to understand more about how this works.

Accepts an optional parameter for page number, useful for resetting the page number to 1.

#### `onField()`

Sets the `$subjectSearchField` property, which is the field used for the `prefetchSubjectIds()` method.

#### `withSourceIds()`

Allows the operation to be scoped to specific source ID strings (e.g. 'person' or 'group', will vary depending on what exactly your Grouper environment uses). Makes use of the `sourceIds` parameter on the API.

*Requires an array of source ID strings*

#### `prefetchSubjectIds()`

Allows the API call to build a mapping of unknown Grouper subject IDs, mapped by a known search field. For instance, I know the user's username, but not their ID. This is potentially necessary since Grouper relies on the Grouper ID being used for any calls that act on Subjects. Here is an example:

```php
$myInstance->prefetchSubjectIds(['jdoe1']);
```

In this example, the interface class will perform a call to `searchSubjects()`. It will perform a search for `jdoe1` on whatever field is set in the `$subjectSearchField` property of the class (default "cn"). It will store the result as an array in the `$prefetchedSubjectsMap` property, as follows:

```php
[
    'jdoe1' => '123456'
]
```

Additionally, it will also store the reverse structure in `$prefetchedSubjectsMapback`:

```
[
    '123456' => 'jdoe1'
]
```

After you perform the lookup of the subject IDs, you can then refer to the subjects later by the same identifier:

```php
$myInstance->getAllGroupsForSubjects(['jdoe1']);
```

Of course, you can also do this fluently in the same chain:

```php
$myInstance->prefetchSubjectIds(['jdoe1'])->getAllGroupsForSubjects(['jdoe1']);
```

These lookup mappings will remain on the instance of the class unless you call `clearPrefetchedSubjects()`.

*Requires an array of lookup strings*

#### `continueOnError()`

Using this method will generally ignore child exceptions of `ApiResultException`. Read the later section on Error Handling for more detail.

#### `clearCallStack()`

Clears the API call stack. Use this if you intend to use the same instance of the `GrouperApiInterface` class to perform multiple operations, and want to separate the call stack out between operations.

#### `clearPrefetchedSubjects()`

Clears out both `$prefetchedSubjectsMap` and `$prefetchedSubjectsMapback` properties, allowing for a clean prefetch (if needed).

#### `withParams()`

Allows manual setting of any first-level parameter in the API call body.

#### `capResultsAt(int)`

Pass an integer to cap the number of results per group (use with `getMembers` calls)

## Paginated API calls

Certain API calls (usually to fetch lists of data) require pagination to ensure that the entire dataset is fetched. Most of the methods that require this are built into the `GrouperApiInterface` class already, but each developer may end up extending and adding more helper methods based on their needs.

If you anticipate requiring pagination, please see the `GrouperApiInterface::getMembersOfGroups()` method for an example of how this is currently being done. Be aware of the setter methods `setPageNumber()` and `setPageSize()` for both customization of the pagination, and also for ensuring that the page number is being incremented properly.

---

# Troubleshooting

## Error Handling

### Exceptions

By default, exceptions are thrown if errors occur with either the technical execution of the API calls (base type `ApiCallFailedException`), or with unexpected results (base type `ApiResultException`). There are many more specific child classes of these two, to help specific situational troubleshooting. Check the `src\Exceptions` folder for included classes.

Generally, it is recommended to use try-catch blocks when interacting with methods of the `GrouperApiInterface` class.

#### Ignoring certain exceptions

Technical API errors (e.g. invalid method types, or other technical failures with an API call) will always throw exceptions. However, the exceptions thrown by seeing unexpected results of the API call can be suppressed by using the chained `continueOnError()` method (see above).

An example: if you `prefetchSubjectIds()` and one of the searches doesn't turn up a result, the default behavior will be to throw a `SubjectNotFoundException` and stop execution of the operation. If, however, you call `continueOnError()->prefetchSubjectIds()`, then the operation will continue. Note that you will not be notified that one of the subject prefetch searches turned up no results, but that is your decision when using the `continueOnError()` directive.

## The Call Stack

The `GrouperApi` class contains a `$callStack` property - an array to which each API result will be appended. This is meant to help in troubleshooting, or for logging purposes. The `$callStack` property can be accessed with the `getCallStack()` getter method.

## Clearing the Call Stack

Note that the call stack is specific per instance of the class. So if you use the same class to conduct multiple transactions, the call stack will include all API calls made for all transactions. If you wish to reset the call stack on the class, you should call the `clearCallStack()` method (chainable).

Here is an example:

```php
use Uncgits\GrouperApi\GrouperApiInterface as Grouper;

$myInstance = new Grouper();

$tree1 = $myInstance->getMemberTree('my-stem:my-group');
print_r($myInstance->getCallStack()); // will contain API calls used to obtain $tree1

$tree2 = $myInstance->getMemberTree('my-stem:my-other-group');
print_r($myInstance->getCallStack()); // will contain API calls used to obtain both $tree1 and $tree2

$tree3 = $myInstance->clearCallStack()->getMemberTree('my-stem:my-other-other-group'); // chaining to clear call stack before request
print_r($myInstance->getCallStack()); // will contain API calls used to obtain $tree3

// alternatively, you could use separate instances:

$instance1 = new Grouper();
$tree1 = $instance1->getMemberTree('my-stem:my-group');
print_r($instance1->getCallStack()); // will contain API calls used to obtain $tree1

$instance2 = new Grouper(); // new instance
$tree2 = $instance2->getMemberTree('my-stem:my-other-group');
print_r($instance2->getCallStack()); // will contain API calls used to obtain $tree2
```

---

# Version History

## 0.7.0

- Open Source BSD license

## 0.6.4

- Further tweak of `findSubjects()` functionality to simply omit users who are not found

## 0.6.3

- Misc cleanup of commented / debugging code
- Removes extra debugging code
- Fixes an issue in `findSubjects()` so that subjects that are not found will be returned as `null` when using `continueOnError()`

## 0.6.2

- fix syntax issue on fixed file. Winning it today.

## 0.6.1

- corrected an issue with the previous commit overwriting the wrong file. Whoops.

## 0.6

- add ability to fetch only a finite number of results via `$maxResults`

## 0.5

- Bump Guzzle dependency to allow 7.*

## 0.4.3

- tweaks functionality of `prefetchSubjectIds` so that we catch the `SubjectNotFoundException` properly, and also properly honor `continueOnError` property. This allows a call to be made and continue if one of the subjects searched is not found.

## 0.4.2

- fix some namespacing issues

## 0.4.1

- sets proxy explicitly to empty string in `$requestOptions` array when not being used, so that we don't fall back on the environment default.

## 0.4.0

- adds `findSubjects()` method to interface for getting single users or lists of specific users

## 0.3.0

- changing `getMemberTree()` so that it uses `getAllMembers()` instead of `getDirectMembers()`

## 0.2.1

- Fixing some commandResult keys for Lite requests
- Prefetch Group IDs
- remove `$replaceExisting` from `deleteMembers()`
- support for flattened member tree
- body parameter TTL fixed in `prefetchSubjectIds()`

## 0.2

- Fixed:
    - some params were not re-passed during pagination
    - Page number was not always get reset during subsequent calls
    - subjectIdentifier was a poor naming choice. renamed to personIdentifier.

- adds `flattenMemberTree()` to helper class.
- enhances error reporting when inaccessible or nonexistent methods are fed to `GrouperApiInterface` and passed on through `__call()` (adds `MethodNotAllowedException` and `MethodNotFoundException` classes)
- stops resetting API object after every call; instead relies on interface class to determine when to reset.
- cleanup of the interface class code - clearer segmentation of methods by purpose.
- adds `commandResult` parameter to API object (for use in determining the return metadata, next bullet)
- improved reporting in the return array from the API object (includes meaningful return code and return message from the API)

## 0.1

- First tagged release
- Includes interface methods for getting members of groups, getting membership details, and getting all groups for subjects. Includes many direct API methods.
