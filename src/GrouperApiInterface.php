<?php

namespace Uncgits\GrouperApi;

use Uncgits\GrouperApi\Exceptions\MembershipNotFoundException;
use Uncgits\GrouperApi\Exceptions\MethodNotAllowedException;
use Uncgits\GrouperApi\Exceptions\MethodNotFoundException;
use Uncgits\GrouperApi\Exceptions\SubjectNotFoundException;
use Uncgits\GrouperApi\Exceptions\GroupNotFoundException;
use Uncgits\GrouperApi\Traits\Chainable;

/**
 * This class will interface with the Grouper API to accomplish tasks. This "middle man" class will provide semantic, task-driven,
 * and logical methods and will do two main things:
 *
 * 1. Understand how to call the Grouper API to complete the request / operation / transaction, and
 * 2. Understand how to parse and communicate the results back to the user in the way they expect.
 *
 */
class GrouperApiInterface
{
    // chaining methods broken out for readability
    use Chainable;

    /**
     * $api - the GrouperApi object
     *
     * @var Uncgits\GrouperApi\GrouperApi
     */
    protected $api;

    /**
     * @var int
     */
    protected $pageNumber = 1;

    /**
     * @var int|null
     */
    protected $maxResults = null;

    /**
     * @var int
     */
    protected $pageSize = 25;

    /**
     * @var array
     */
    protected $subjectAttributeNames = [];

    /**
     * @var string
     */
    protected $subjectSearchField = 'cn'; // default value

    /**
     * @var array
     */
    protected $prefetchedSubjectsMap = [];

    /**
     * @var array
     */
    protected $prefetchedSubjectsMapback = [];

    /**
     * @var array
     */
    protected $prefetchedGroupsMap = [];

    /**
     * @var array
     */
    protected $prefetchedGroupsMapback = [];

    /**
     * @var array
     */
    protected $personIdentifiers = [];

    /**
     * @var array
     */
    protected $groupIdentifiers = [];

    /**
     * @var bool
     */
    protected $continueOnError = false;

    /**
     * GrouperApiInterface constructor.
     */
    public function __construct()
    {
        // Instantiate API object
        $this->api = new GrouperApi;
    }

    /**
     * @param $name
     * @param $arguments
     *
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        // enable more flexible interaction with API class. if method does not exist on this class, delegate calls to API class (if allowed)
        if (method_exists($this->api, $name)) {
            if (is_callable([$this->api, $name])) {
                return call_user_func_array([$this->api, $name], $arguments);
            }
            throw new MethodNotAllowedException("Method '$name' is private or protected (not callable on GrouperApiInterface or GrouperApi models in this context).");
        }

        throw new MethodNotFoundException("Method '$name' does not exist on GrouperApiInterface or GrouperApi models.");
    }

    /*
    |--------------------------------------------------------------------------
    | Setters
    |--------------------------------------------------------------------------
     */

    /**
     * @param int $pageNumber
     */
    public function setPageNumber(int $pageNumber)
    {
        $this->pageNumber = $pageNumber;
    }

    /**
     * @param $pageSize
     */
    public function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @param array $names
     */
    public function setSubjectAttributeNames(array $names)
    {
        $this->subjectAttributeNames = $names;
    }

    /**
     * @param string $searchField
     */
    public function setSubjectSearchField(string $searchField)
    {
        $this->subjectSearchField = $searchField;
    }

    /**
     * @param array $identifiers
     */
    public function setPersonIdentifiers(array $identifiers)
    {
        $this->personIdentifiers = $identifiers;
    }

    /**
     * @param array $groupIdentifiers
     */
    public function setGroupIdentifiers(array $groupIdentifiers)
    {
        $this->groupIdentifiers = $groupIdentifiers;
    }

    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
     */

    protected function explodeFullGroupName($group)
    {
        $exploded = explode(':', $group);
        $groupName = array_pop($exploded);
        $stem = implode(':', $exploded);

        return [$stem, $groupName];
    }

    /**
     * @param $searchString
     * @param $searchField
     *
     * @return mixed
     */
    protected function searchSubject($searchString, $searchField)
    {
        $this->setParams([
            'searchString' => $searchString,
        ]);

        $result = $this->withSubjectDetail()
            ->withSubjectAttributeNamesString()
            ->getSubjectsLite();

        $resultBody = $result['body']->WsGetSubjectsResults;
        $attributeNames = $resultBody->subjectAttributeNames;

        if (isset($resultBody->wsSubjects)) {
            foreach ($resultBody->wsSubjects as $subject) {
                $attributesArray = array_combine($attributeNames, $subject->attributeValues);
                if (isset($attributesArray[$searchField]) && strtolower($attributesArray[$searchField]) == strtolower($searchString)) {
                    unset($subject->attributeValues);
                    $subject->attributes = $attributesArray;
                    return $subject;
                }
            }
        }

        throw (new SubjectNotFoundException("Could not find match for subject '$searchString' on field '$searchField'."))
            ->withCallStack($this->getCallStack());
    }

    /**
     * @param $group
     * @param $searchField
     *
     * @return mixed
     */
    protected function searchGroup($group)
    {
        list($stem, $group) = $this->explodeFullGroupName($group);

        $result = $this->withGroupDetail()
            ->findGroups($stem, $group, 'FIND_BY_GROUP_NAME_EXACT');

        $resultBody = $result['body']->WsFindGroupsResults;

        if (isset($resultBody->groupResults)) {
            return $resultBody->groupResults[0];
        }

        throw (new GroupNotFoundException("Could not find exact name match for group '$stem:$group'."))
            ->withCallStack($this->getCallStack());
    }

    /**
     * @param $groupsWithStems
     *
     * @return mixed
     */
    protected function getMembersOfGroups($groupsWithStems)
    {
        $this->setPageNumber(1);
        $membersByGroup = [];

        if (!is_array($groupsWithStems)) {
            $groupsWithStems = [$groupsWithStems];
        }

        // make sure we fetch only enough results to comply with $maxResults
        if (!is_null($this->maxResults)) {
            if ($this->pageSize > count($groupsWithStems) * $this->maxResults) {
                $this->pageSize = count($groupsWithStems) * $this->maxResults;
            }
        }

        $totalResults = 0;
        do {
            $this->withPagination();

            $emptyResults = 0;
            // TODO - need to make sure other params are still accounted for when making the paginated calls (e.g. memberFilter)
            $result = $this->withSubjectDetail()
                ->getMembers($groupsWithStems);
            $memberResults = $result['body']->WsGetMembersResults->results;

            $groups = $result['meta']['body_parameters']['wsGroupLookups'];

            foreach ($memberResults as $key => $memberResult) {
                if (!$this->continueOnError) {
                    if ($memberResult->resultMetadata->resultCode == 'GROUP_NOT_FOUND') {
                        $groupName = $groups[$key]['groupName'];
                        throw new GroupNotFoundException("Could not find match for group '$groupName'.");
                    }
                }

                if (!isset($memberResult->wsSubjects)) {
                    $emptyResults++;
                    continue;
                }

                $groupId = $memberResult->wsGroup->name;

                if (isset($membersByGroup[$groupId])) {
                    $membersByGroup[$groupId] = array_merge($membersByGroup[$groupId], $memberResult->wsSubjects);
                } else {
                    $membersByGroup[$groupId] = $memberResult->wsSubjects;
                }
                $totalResults += count($memberResult->wsSubjects);

                $this->pageNumber++;
            }
        } while ($emptyResults < count($memberResults) && $totalResults < ($this->maxResults ?? 999999) * count($groupsWithStems));

        return $membersByGroup;
    }

    /**
     * @param $subjects
     *
     * @return array|mixed
     */
    protected function mapSubjects($subjects)
    {
        $returnType = 'array';

        if (!is_array($subjects)) {
            $returnType = 'string';
            $subjects = [$subjects];
        }

        $lookupArray = [];

        foreach ($subjects as $subject) {
            $subject = strtolower($subject);
            if (isset($this->prefetchedSubjectsMap[$subject])) {
                $lookupArray[] = $this->prefetchedSubjectsMap[$subject];
            } else {
                $lookupArray[] = $subject;
            }
        }

        return $returnType == 'array' ? $lookupArray : $lookupArray[0];
    }

    /*
    |--------------------------------------------------------------------------
    | Callables (exposed methods)
    |--------------------------------------------------------------------------
     */

    // ========================================
    // MEMBERS
    // ========================================

    /**
     * @param $groupsWithStems
     *
     * @return mixed
     */
    public function getAllMembers($groupsWithStems)
    {
        $members = $this->withMemberFilter('all')
            ->getMembersOfGroups($groupsWithStems);

        $this->reset();
        return $members;
    }

    /**
     * @param $groupsWithStems
     *
     * @return mixed
     */
    public function getDirectMembers($groupsWithStems)
    {
        $members = $this->withMemberFilter('immediate')
            ->getMembersOfGroups($groupsWithStems);

        $this->reset();
        return $members;
    }

    /**
     * @param $groupsWithStems
     *
     * @return mixed
     */
    public function getIndirectMembers($groupsWithStems)
    {
        $members = $this->withMemberFilter('nonimmediate')
            ->getMembersOfGroups($groupsWithStems);

        $this->reset();
        return $members;
    }

    /**
     * @param $groupsWithStems
     *
     * @return mixed
     */
    public function getChildGroups($groupsWithStems)
    {
        $members = $this->withSourceIds($this->groupIdentifiers)
            ->withMemberFilter('immediate')
            ->getMembersOfGroups($groupsWithStems);

        $this->reset();
        return $members;
    }

    /**
     * @param $groupWithStem
     *
     * @return array
     */
    public function getMemberTree($groupWithStem, $flattened = false)
    {
        $this->setPageNumber(1);

        $memberTree = [];

        $result = $this->withSubjectAttributeNames($this->subjectAttributeNames)
            ->getAllMembers("$groupWithStem"); // this returns a nice array already, and accomplishes our pagination.

        foreach ($result as $subjects) {
            foreach ($subjects as $subject) {
                $memberTree[$groupWithStem][] = in_array(
                    $subject->sourceId,
                    $this->personIdentifiers
                ) ? $subject : $this->getMemberTree($subject->name);
            }
        }

        $this->reset();

        if (!$flattened) {
            return $memberTree;
        }

        $memberTreeFlattened = [];
        array_walk_recursive($memberTree, function ($member) use (&$memberTreeFlattened) {
            $memberTreeFlattened[] = $member;
        });

        return $memberTreeFlattened;
    }

    /**
     * @param array $subjectIds
     * @param $stem
     * @param $group
     * @param bool $replaceExisting
     *
     * @return mixed
     */
    public function addMembers(array $subjectIds, $stem, $group, $replaceExisting = false)
    {
        $subjectIds = $this->mapSubjects($subjectIds);
        return $this->addMember($subjectIds, $stem, $group, $replaceExisting);
    }

    /**
     * @param array $subjectIds
     * @param $stem
     * @param $group
     *
     * @return mixed
     */
    public function deleteMembers(array $subjectIds, $stem, $group)
    {
        $subjectIds = $this->mapSubjects($subjectIds);
        return $this->deleteMember($subjectIds, $stem, $group);
    }

    /**
     * @param array $subjectIds
     * @param $stem
     * @param $group
     *
     * @return mixed
     */
    public function syncMembers(array $subjectIds, $stem, $group)
    {
        return $this->addMembers($subjectIds, $stem, $group, true);
    }

    // ========================================
    // MEMBERSHIPS
    // ========================================

    /**
     * @param $membershipIds
     *
     * @return mixed
     */
    public function getMembershipDetailsById($membershipIds)
    {
        if (!is_array($membershipIds)) {
            $membershipIds = [$membershipIds];
        }

        $params = [
            'membershipIds' => $membershipIds
        ];
        $result = $this->withSubjectDetail()
            ->withGroupDetail()
            ->withSubjectAttributeNames()
            ->getMemberships($params);

        $membershipsResult = $result['body']->WsGetMembershipsResults;


        if (!isset($membershipsResult->wsMemberships)) {
            throw new MembershipNotFoundException('No memberships found for the membership ID(s): ' . implode(
                    ', ',
                    $membershipIds
                ));
        }

        $memberships = [];

        // reindex subjects by id
        $subjects = [];
        foreach ($membershipsResult->wsSubjects as $subject) {
            $subjects[$subject->id] = $subject;
        }
        // reindex groups by name
        $groups = [];
        foreach ($membershipsResult->wsGroups as $group) {
            $groups[$group->displayName] = $group;
        }


        // parse
        foreach ($membershipsResult->wsMemberships as $membership) {
            $membership->subject = $subjects[$membership->subjectId];
            $membership->group = $groups[$membership->groupName];

            $memberships[$membership->membershipId] = $membership;
        }

        $this->reset();
        return $memberships;
    }

    // ========================================
    // GROUPS
    // ========================================

    /**
     * @param $subjectIds
     *
     * @return mixed
     */
    public function getAllGroupsForSubjects($subjectIds)
    {
        $subjectIds = $this->mapSubjects($subjectIds);

        $groupsBySubject = [];
        $this->setPageNumber(1);

        do {
            // add pagination params
            $this->withPagination();

            $emptyResults = 0;
            $result = $this->getGroups($subjectIds);
            $groupResults = $result['body']->WsGetGroupsResults->results;
            foreach ($groupResults as $groupResult) {
                if (!isset($groupResult->wsGroups)) {
                    $emptyResults++;
                    continue;
                }

                $subjectId = $groupResult->wsSubject->id;
                if (isset($this->prefetchedSubjectsMapback[$subjectId])) {
                    $subjectId = $this->prefetchedSubjectsMapback[$subjectId]; // return as username if that's what was given.
                }

                if (isset($groupsBySubject[$subjectId])) {
                    $groupsBySubject[$subjectId] = array_merge($groupsBySubject[$subjectId], $groupResult->wsGroups);
                } else {
                    $groupsBySubject[$subjectId] = $groupResult->wsGroups;
                }
            }

            $this->pageNumber++;
        } while ($emptyResults < count($groupResults));

        $this->reset();
        return $groupsBySubject;
    }

    // ========================================
    // SUBJECTS
    // ========================================

    public function findSubjects($subjectIds, $sourceId = null)
    {
        if (!is_array($subjectIds)) {
            $subjectIds = [$subjectIds];
        }

        $subjectIds = $this->mapSubjects($subjectIds);
        $result = $this->withSubjectAttributeNames()
            ->withSubjectDetail()
            ->getSubjects($subjectIds, $sourceId);

        if ($result['response']['result'] == 'SUCCESS') {
            $subjectAttributeNames = $result['body']->WsGetSubjectsResults->subjectAttributeNames;
            $subjects = [];
            foreach ($result['body']->WsGetSubjectsResults->wsSubjects as $subject) {
                $subjectId = $subject->id;
                if (isset($this->prefetchedSubjectsMapback[$subjectId])) {
                    $subjectId = $this->prefetchedSubjectsMapback[$subjectId]; // return as username if that's what was given.
                }
                if ($subject->success === 'T') {
                    $mapped = array_combine($subjectAttributeNames, $subject->attributeValues);
                    $subjects[$subjectId] = $mapped;
                }
            }

            return $subjects;
        }

        throw new ApiException('Failed API call: ' . $result['response']['resultMessage']);
    }
}
