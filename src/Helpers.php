<?php

namespace Uncgits\GrouperApi;

class Helpers
{
    /**
     * @param $memberTree
     * @param array $flattenedMemberTree
     *
     * @return array
     */
    public function flattenMemberTree($memberTree, &$flattenedMemberTree = [])
    {
        foreach ($memberTree as $master => $members) {
            foreach ($members as $member) {
                if (is_array($member)) {
                    // it's a group
                    $this->flattenMemberTree($member, $flattenedMemberTree);
                } else {
                    $flattenedMemberTree[$master][] = $member;
                }
            }
        }

        return $flattenedMemberTree;
    }

    /**
     * getNewSaltedPassword() - creates a random password and returns it, and the components used to make it.
     *
     * @return array
     */
    public static function getNewSaltedPassword()
    {
        $bytes = openssl_random_pseudo_bytes(32, $cstrong);
        $salt = bin2hex($bytes);

        $bytes = openssl_random_pseudo_bytes(12, $cstrong);
        $rawPass = bin2hex($bytes);

        $password = hash("sha256", $salt . $rawPass);

        return array(
            "rawPassword"    => $rawPass,
            "salt"           => $salt,
            "hashedPassword" => $password
        );
    }

    /**
     * secondsToTime() - converts a number of seconds (such as that returned by the Grouper API for recordings) and converts to hh:mm:ss for display
     *
     * @param $original
     *
     * @return string
     */
    public static function secondsToTime($original)
    {
        $hours = floor($original / (60 * 60));
        $minutes = floor(($original / 60) % 60);
        $seconds = floor($original % 60);

        return sprintf("%02d:%02d:%02d", $hours, $minutes, $seconds);
    }
}