<?php

namespace Uncgits\GrouperApi;

use GuzzleHttp\Client;
use Uncgits\GrouperApi\Exceptions\UnsupportedHttpMethodException;
use Uncgits\GrouperApi\Exceptions\MissingPropertyException;

const API_VERSION = 'v2_2_000';
const API_SUFFIX_PATH = 'grouper-ws/servicesRest/json';

/**
 * Class GrouperApi
 * @package Uncgits\GrouperApi
 */
class GrouperApi
{
    /*
    |--------------------------------------------------------------------------
    | Notes on Grouper API
    |--------------------------------------------------------------------------
    |
    | Many requests have a "lite" version and a "non-lite" version. For instance,
    | the 'getMembers' function has a 'getMembers' and 'getMembersLite'. The main
    | deciding factor in which one gets called is the method used. They use the
    | same endpoint, but often if you use GET then the Lite version is called,
    | whereas if you use POST the full version is called. If you use POST then
    | you need to be prepared to provide a JSON body with the request
    |
    | Supported call methods:
    | - GET (mostly for Lite operations)
    | - POST (for lots of things - full "gets", deletes, some edits, some adds)
    | - PUT (mostly for edits)
    | - DELETE (for deleting using Lite service)
    |
    | Also, many Full requests seem to allow for bulk lookup (e.g. get group
    | memberships for many users, not just one). Good to remember for efficiency.
    |
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Properties
    |--------------------------------------------------------------------------
    |
    | For abstraction these properties will hold the relevant environment and
    | auth info. Set these in a wrapper or whatever other framework you're
    | using.
    |
    */

    /**
     * $host - the host endpoint to use for API calls. Do not include protocol (https://)
     *
     * @var string
     */
    private $host;

    /**
     * $username - the username to use for API calls
     *
     * @var string
     */
    private $username;

    /**
     * $password - the password to use for API calls
     *
     * @var string
     */
    private $password;

    /**
     * $applicationName - the name of the application used to make the API call (for HTTP headers)
     *
     * @var string
     */
    private $applicationName;

    /**
     * $proxyHost - the host for an HTTP proxy
     *
     * @var string
     */
    private $proxyHost;

    /**
     * $proxyPort - the port to use for an HTTP proxy
     *
     * @var string
     */
    private $proxyPort;

    /**
     * $useProxy - whether to use an HTTP proxy
     *
     * @var boolean
     */
    private $useProxy = false;

    /**
     * $body - JSON-encoded string that contains the body of the API call
     *
     * @var string
     */
    protected $body = null;

    /**
     * $command - the name of the API command used for a POST request (used to generate the request body)
     *
     * @var string
     */
    protected $command = null;

    /**
     * $commandResult - the key for the result handed back by the API call
     *
     * @var string
     */
    protected $commandResult = null;

    /**
     * $params - the parameters used in the call (used to generate the request body)
     *
     * @var array
     */
    protected $params = null;

    /**
     * $callStack - a history of API calls made during this class instance's lifespan
     *
     * @var array
     */
    protected $callStack = [];

    /*
    |--------------------------------------------------------------------------
    | Setters
    |--------------------------------------------------------------------------
    */

    /**
     * setHost - setter for $host
     *
     * @param mixed $host
     *
     * @return void
     */
    public function setHost($host)
    {
        $this->host = $host . '/' . API_SUFFIX_PATH . '/' . API_VERSION;
    }

    /**
     * setUsername - setter for $username
     *
     * @param mixed $username
     *
     * @return void
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * setPassword - setter for $password
     *
     * @param mixed $password
     *
     * @return void
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * setApplicationName - setter for $applicationName
     *
     * @param mixed $applicationName
     *
     * @return void
     */
    public function setApplicationName($applicationName)
    {
        $this->applicationName = $applicationName;
    }

    /**
     * setProxyHost - setter for $proxyHost
     *
     * @param mixed $proxyHost
     *
     * @return void
     */
    public function setProxyHost($proxyHost)
    {
        $this->proxyHost = $proxyHost;
    }

    /**
     * setProxyPort - setter for $proxyPort
     *
     * @param mixed $proxyPort
     *
     * @return void
     */
    public function setProxyPort($proxyPort)
    {
        $this->proxyPort = $proxyPort;
    }

    /**
     * setUseProxy - setter for $useProxy
     *
     * @param mixed $useProxy
     *
     * @return void
     */
    public function setUseProxy($useProxy)
    {
        $this->useProxy = ($useProxy === true);
    }

    /**
     * setCommand - setter for $command
     *
     * @param mixed $command
     *
     * @return void
     */
    public function setCommand($command)
    {
        $this->command = $command;

        // try to formulaically set the commandResult string based on common convention
        $commandResult = str_replace('Rest', '', $command);

        $resultString = strpos($command, 'Lite') === false ? 'Results' : 'Result';

        $commandResult = str_replace('Request', $resultString, $commandResult);
        $this->setCommandResult($commandResult);
    }

    /**
     * setCommandResult - setter for $commandResult
     *
     * @param mixed $commandResult
     *
     * @return void
     */
    public function setCommandResult($commandResult)
    {
        $this->commandResult = $commandResult;
    }

    /**
     * setParams - setter for $params - minor processing to be sure of array structure
     *
     * @param array $params
     *
     * @return void
     */
    public function setParams(array $params)
    {
        foreach ($params as $key => $value) {
            $this->params[$key] = $value;
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Getters
    |--------------------------------------------------------------------------
    |
    */

    /**
     * getCallStack - getter for $callStack
     *
     * @return array
     */
    public function getCallStack()
    {
        return $this->callStack;
    }

    /*
    |--------------------------------------------------------------------------
    | Helper Methods
    |--------------------------------------------------------------------------
    |
    | Methods to help set properties, etc., shared across multiple API calls
    |
    */

    /**
     * setBody - structures the request body array and encodes it into JSON
     *      utilizes $this->command and $this->params
     *
     * @return void
     */
    public function setBody()
    {
        $body = [
            $this->command => $this->params
        ];

        $this->body = json_encode($body);
    }

    /**
     * getLookupsArray - generates a lookup array for the request body according to the format used by the API
     *
     * @param mixed $items
     * @param mixed $key
     *
     * @return void
     */
    protected function getLookupsArray($items, $key)
    {
        if (!is_array($items)) {
            $items = [$items];
        }

        $lookups = [];

        foreach ($items as $item) {
            $lookups[] = [
                $key => $item
            ];
        }

        return $lookups;
    }

    /**
     * If being used as a singleton, this resets the call parameters so that we're ready for a new one.
     */
    public function reset()
    {
        $this->body = null;
        $this->command = null;
        $this->params = null;
    }

    public function clearCallStack()
    {
        $this->callStack = [];
    }

    /*
    |--------------------------------------------------------------------------
    | API Calls
    |--------------------------------------------------------------------------
    |
    | Methods that actually perform the API calls to Grouper APIs. Not callable
    | directly; only internally by other class methods.
    |
    */

    /**
     * apiCall() - performs a call to the Grouper JSON REST API
     * https://my.grouper.url/{{endpoint}}
     *
     * @param string $method - HTTP request method: GET, POST, PATCH, DELETE, etc.
     * @param string $endpoint - where to send the request
     * @param $operation
     *
     * @return array
     * @throws \Uncgits\GrouperApi\Exceptions\ApiCallException
     */
    protected function apiCall($method, $endpoint)
    {
        // check that object has been set up properly.
        $requiredProperties = ['host', 'username', 'password', 'applicationName'];
        foreach ($requiredProperties as $property) {
            if ($this->$property === null) {
                throw new MissingPropertyException("Error: required property '$property' has not been set in API object.");
            }
        }

        // check method
        $method = strtolower($method);

        if (!in_array($method, ['get', 'post', 'put', 'delete'])) {
            throw new UnsupportedHttpMethodException("Error: HTTP method '$method' is unsupported or invalid.");
        }

        // assemble the request target and endpoint
        $endpoint = "https://{$this->host}/$endpoint";

        // instantiate Guzzle client with headers
        $client = new Client([
            'http_errors' => false
        ]);

        // set up basic options with HTTP Basic auth
        $requestOptions = [
            'auth' => [
                $this->username,
                $this->password
            ]
        ];

        // set body if we are doing a request with a body (PUT, POST)
        if (!empty($this->body)) {
            $requestOptions['body'] = $this->body;
            $requestOptions['headers']['User-Agent'] = $this->applicationName;
            $requestOptions['headers']['Content-Type'] = 'text/x-json; charset=UTF-8';
        }

        // use proxy if it is set in .env
        if ($this->useProxy) {
            $requestOptions['proxy'] = $this->proxyHost . ':' . $this->proxyPort;
        } else {
            $requestOptions['proxy'] = '';
        }

        // perform the call
        $response = $client->$method($endpoint, $requestOptions);

        // get response data
        $code = $response->getStatusCode();
        $reason = $response->getReasonPhrase();
        $body = $response->getBody();
        $bodyContents = json_decode($body->getContents()); // parse JSON response to PHP object
        // dd($bodyContents);

        // if ($this->command == 'WsRestGetSubjectsLiteRequest') {
        //     dd($this, $bodyContents);
        // }

        // return raw data about the request so that it can be parsed by the calling function
        $callResult = [
            'meta'     => [
                'time_attempted'  => time(),
                'endpoint'        => $endpoint,
                'command'         => $this->command,
                'body_parameters' => $this->params,
                'method'          => $method,
            ],
            'response' => [
                'result'        => $code < 300 ? 'SUCCESS' : 'FAILURE',
                'httpCode'      => $code,
                'resultCode'    => is_null($bodyContents) ? null : $bodyContents->{$this->commandResult}->resultMetadata->resultCode,
                'resultMessage' => is_null($bodyContents) ? null : $bodyContents->{$this->commandResult}->resultMetadata->resultMessage,
            ],
            'body'     => $bodyContents
        ];

        $this->callStack[] = $callResult;

        // $this->reset(); // in case more calls are made

        return $callResult;
    }

    /*
    |--------------------------------------------------------------------------
    | Grouper API methods
    |--------------------------------------------------------------------------
    |
    | Direct analog methods for doing stuff with the Grouper REST API
    |
    */

    /**
     * getMembersLite - a getMembersLite request
     * https://spaces.internet2.edu/display/Grouper/Get+Members
     *
     * @param mixed $stem
     * @param mixed $group
     *
     * @return void
     */
    public function getMembersLite($stem, $group)
    {
        return $this->apiCall('get', "groups/$stem:$group/members");
    }

    /**
     * getMembers - a getMembers request
     * https://spaces.internet2.edu/display/Grouper/Get+Members
     *
     * @param mixed $groupsWithStem
     *
     * @return void
     */
    public function getMembers($groupsWithStem)
    {
        // NOTES: this only does direct members at this time. Must be a filter to do indirect.
        $this->setCommand('WsRestGetMembersRequest');

        $lookups = $this->getLookupsArray($groupsWithStem, 'groupName');

        $this->setParams(['wsGroupLookups' => $lookups]);
        $this->setBody();

        return $this->apiCall('post', 'groups');
    }

    /**
     * getGroupsLite - a getGroupsLite request
     * https://spaces.internet2.edu/display/Grouper/Get+Groups
     *
     * @param mixed $subjectId
     *
     * @return void
     */
    public function getGroupsLite($subjectId)
    {
        $this->setCommand('WsRestGetGroupsLiteRequest');
        return $this->apiCall('get', "subjects/$subjectId/groups");
    }

    /**
     * getGroups - a getGroups request
     * https://spaces.internet2.edu/display/Grouper/Get+Groups
     *
     * @param mixed $subjectIds
     *
     * @return void
     */
    public function getGroups($subjectIds)
    {
        // Oddly returns a 201 - CREATED code...
        $this->setCommand('WsRestGetGroupsRequest');

        $lookups = $this->getLookupsArray($subjectIds, 'subjectId');

        // TODO - custom body
        $this->setParams(['subjectLookups' => $lookups]);
        $this->setBody();

        return $this->apiCall('post', 'subjects');
    }

    /**
     * getSubjectsLite - a getSubjectsLite request
     * https://spaces.internet2.edu/display/Grouper/Get+Subjects
     *
     * @param mixed $subjectId
     * @param mixed $sourceId
     *
     * @return void
     */
    public function getSubjectsLite($subjectId = null, $sourceId = null)
    {
        // this must be done as Lite
        $this->setCommand('WsRestGetSubjectsLiteRequest');
        $this->setCommandResult('WsGetSubjectsResults');

        $params = [];
        if (!is_null($subjectId)) {
            $params['subjectId'] = $subjectId;
        }
        if (!is_null($sourceId)) {
            $params['sourceId'] = $sourceId;
        }

        $this->setParams($params);
        $this->setBody();

        return $this->apiCall('post', 'subjects');
    }

    /**
     * getSubjects - a getSubjects request
     * https://spaces.internet2.edu/display/Grouper/Get+Subjects
     *
     * @param mixed $subjectIds
     * @param mixed $sourceId
     *
     * @return void
     */
    public function getSubjects($subjectIds, $sourceId = null)
    {
        $this->setCommand('WsRestGetSubjectsRequest');

        $params = ['wsSubjectLookups'];
        foreach ($subjectIds as $subjectId) {
            $subjectArray = ['subjectId' => $subjectId];
            if (!is_null($sourceId)) {
                $subjectArray['subjectSourceId'] = $sourceId;
            }
            $params['wsSubjectLookups'][] = $subjectArray;
        }

        $this->setParams($params);
        $this->setBody();

        return $this->apiCall('post', 'subjects');
    }

    /**
     * getMembershipsLite - a getMembershipsLite request
     * https://spaces.internet2.edu/display/Grouper/Get+Memberships
     *
     * @param mixed $stem
     * @param mixed $group
     *
     * @return void
     */
    public function getMembershipsLite($stem, $group)
    {
        // https://cdn.rawgit.com/Internet2/grouper/master/grouper-ws/grouper-ws/doc/api/edu/internet2/middleware/grouper/ws/rest/membership/WsRestGetMembershipsLiteRequest.html?view=co
        return $this->apiCall('get', "groups/$stem:$group/memberships");
    }

    /**
     * getMemberships - a getMemberships request
     * https://spaces.internet2.edu/display/Grouper/Get+Memberships
     *
     * @param mixed $stem
     * @param mixed $group
     * @param array $bodyParams
     *
     * @return void
     */
    public function getMemberships(array $bodyParams = [])
    {
        // This will be the heavy lifter.
        // THIS CALL IS INTERESTING. Lots of options here. It can do:
        // - get a membership by its membership ID
        // - look in a subset of groups
        // - look in a subset of subjects
        // - look for memberships in all groups on a particular stem

        // I think lots of the work will need to be done in the Interface class, since there is so much flexibility.

        // https://cdn.rawgit.com/Internet2/grouper/master/grouper-ws/grouper-ws/doc/api/edu/internet2/middleware/grouper/ws/rest/membership/WsRestGetMembershipsRequest.html?view=co

        /* To explore...many of these can be used on the Lite requests as well, theoretically.

            fieldName => if the memberships should be retrieved from a certain field membership of the group (certain list)
            fieldType => the type of field to look at
            memberFilter => must be one of All, Effective, Immediate, Composite, NonImmediate
            membershipIds => the membership ids to search for if they are known
            scope => a sql like string which will have a percent % concatenated to the end for group names to search in (or stem names)
            stemScope => to search only in one stem or in substems: ONE_LEVEL, ALL_IN_SUBTREE
            subjectAttributeNames => the additional subject attributes (data) to return.
            wsGroupLookups => the groups to look in
            wsOwnerAttributeDefLookups => attribute definition lookups if looking for memberships on certain attribute definitions
            wsStemLookup => the (singular) stem lookup if looking for membership on certain stems
            wsSubjectLookups => subjects to look in
            wsOwnerStemLookups => not as well documented, but seems to return privileges for the user in those stems


        */

        // This has lots of potential because each membership will show a unique ID to the group, but also show its indirect relationship as well (e.g. if I am a member of X through Y, my membership to X is indirect and has its own ID, but the API will also say "hey, here's the membership ID of him+Y as well."

        $this->setCommand('WsRestGetMembershipsRequest');

        // $body = [
        //     'WsRestGetMembershipsRequest' => [
//                "membershipIds" => [
//                    "61f8116ebbcc412a86c06424e492f323:44cba3f2d290409da2423bb6a5061054"
//                ],
//                "wsGroupLookups" => [
//                    [
//                        "groupName" => "$stem:$group"
//                    ]
//                ]
//                "wsSubjectLookups" => [
//                    [
//                        "subjectId" => "334263"
//                    ]
//                ],
//                "wsStemLookup" => "$stem" // this is the way to do this.
        //     ]
        // ];

        // set additional params
        $this->setParams($bodyParams);
        $this->setBody();

        return $this->apiCall('post', 'memberships');
    }

    /**
     * addMemberLite - an addMemberLite request
     * https://spaces.internet2.edu/display/Grouper/Add+Member
     *
     * @param mixed $subjectId
     * @param mixed $stem
     * @param mixed $group
     *
     * @return void
     */
    public function addMemberLite($subjectId, $stem, $group)
    {
        $this->setCommand('WsRestAddMemberLiteRequest');
        $this->setCommandResult('WsAddMemberLiteResult');

        return $this->apiCall('put', "groups/$stem:$group/members/$subjectId");
    }

    /**
     * addMember - an addMember request
     * https://spaces.internet2.edu/display/Grouper/Add+Member
     *
     * @param mixed $subjectIds
     * @param mixed $stem
     * @param mixed $group
     * @param mixed $replaceExisting
     *
     * @return void
     */
    public function addMember($subjectIds, $stem, $group, $replaceExisting = false)
    {
        $this->setCommand('WsRestAddMemberRequest');

        $params = [
            'replaceAllExisting' => $replaceExisting ? 'T' : 'F',
            'subjectLookups'     => $this->getLookupsArray($subjectIds, 'subjectId')
        ];

        $this->setParams($params);
        $this->setBody();

        return $this->apiCall('put', "groups/$stem:$group/members");
    }

    /**
     * deleteMemberLite - a deleteMemberLite request
     * https://spaces.internet2.edu/display/Grouper/Delete+Member
     *
     * @param mixed $subjectId
     * @param mixed $stem
     * @param mixed $group
     *
     * @return void
     */
    public function deleteMemberLite($subjectId, $stem, $group)
    {
        $this->setCommand('WsRestDeleteMemberLiteRequest');

        return $this->apiCall('delete', "groups/$stem:$group/members/$subjectId");
    }

    /**
     * deleteMember - a deleteMember request
     * https://spaces.internet2.edu/display/Grouper/Delete+Member
     *
     * @param mixed $subjectIds
     * @param mixed $stem
     * @param mixed $group
     *
     * @return void
     */
    public function deleteMember($subjectIds, $stem, $group)
    {
        $this->setCommand('WsRestDeleteMemberRequest');

        $lookups = $this->getLookupsArray($subjectIds, 'subjectId');

        $this->setParams(['subjectLookups' => $lookups]);
        $this->setBody();

        return $this->apiCall('delete', "groups/$stem:$group/members");
    }

    /**
     * groupSaveLite - a groupSaveLite request
     * https://spaces.internet2.edu/display/Grouper/Group+Save
     *
     * @param mixed $stem
     * @param mixed $group
     * @param mixed $description
     * @param mixed $extraParams
     *
     * @return void
     */
    public function groupSaveLite($stem, $group, $description, $extraParams = [])
    {
        $this->setCommand('WsRestGroupSaveLiteRequest');
        $params = [
            'description' => $description,
            'groupName'   => "$stem:$group"
        ];

        // additional
        foreach ($extraParams as $key => $value) {
            $params[$key] = $value;
        }

        $this->setParams($params);
        $this->setBody();

        return $this->apiCall('post', "groups/$stem:$group");
    }

    /**
     * groupSave - a groupSave request
     * https://spaces.internet2.edu/display/Grouper/Group+Save
     *
     * @param mixed $groupInfo
     * @param mixed $extraParams
     *
     * @return void
     */
    public function groupSave($groupInfo, $extraParams = [])
        // $groupInfo should be arrays of key-value pairs of valid group properties
    {
        // TODO - look at group detail: https://github.com/Internet2/grouper/blob/master/grouper-ws/grouper-ws/doc/samples/groupSave/WsSampleGroupDetailSaveRest_withDetail_json.txt

        if (!is_array($groupInfo)) {
            $groupInfo = [$groupInfo];
        }

        $this->setCommand('WsRestGroupSaveRequest');

        $groups = [];

        foreach ($groupInfo as $name => $group) {
            if (!isset($group['name'])) {
                $group['name'] = $name;
            }

            $groupBlock = [
                'wsGroup'       => $group,
                'wsGroupLookup' => $name,
            ];

            $groups[] = $groupBlock;
        }

        $params = array_merge(['wsGroupToSaves' => $groups], $extraParams);

        $this->setParams($params);
        $this->setBody();

        return $this->apiCall('post', 'groups');
    }

    /**
     * groupDeleteLite - a groupDeleteLite request
     * https://spaces.internet2.edu/display/Grouper/Group+Delete
     *
     * @param mixed $stem
     * @param mixed $group
     *
     * @return void
     */
    public function groupDeleteLite($stem, $group)
    {
        return $this->apiCall('delete', "groups/$stem:$group");
    }

    /**
     * groupDelete - a groupDelete request
     * https://spaces.internet2.edu/display/Grouper/Group+Delete
     *
     * @param mixed $groupsArray
     *
     * @return void
     */
    public function groupDelete($groupsArray)
    {
        $this->setCommand('WsRestGroupDeleteRequest');

        $lookups = $this->getLookupsArray($groupsArray, 'groupName');

        $this->setParams(['wsGroupLookups' => $lookups]);
        $this->setBody();

        return $this->apiCall('post', 'groups');
    }

    /**
     * findGroupsLite - a findGroupsLite request
     * https://spaces.internet2.edu/display/Grouper/Find+Groups
     *
     * @param mixed $stem
     * @param mixed $group
     *
     * @return void
     */
    public function findGroupsLite($stem, $group)
    {
        $this->setCommand('WsRestFindGroupsLiteRequest');

        $params = [
            'groupName'       => $group,
            'stemName'        => $stem,
            'queryFilterType' => 'FIND_BY_GROUP_NAME_APPROXIMATE'
        ];

        $this->setParams($params);
        $this->setBody();

        return $this->apiCall('post', 'groups');
    }

    /**
     * findGroups - a findGroups request
     * https://spaces.internet2.edu/display/Grouper/Find+Groups
     *
     * @param mixed $stem
     * @param mixed $group
     *
     * @return void
     */
    public function findGroups($stem, $group, $filter = 'FIND_BY_GROUP_NAME_APPROXIMATE')
    {
        // valid filters: FIND_BY_GROUP_UUID, FIND_BY_GROUP_NAME_EXACT, FIND_BY_STEM_NAME, FIND_BY_APPROXIMATE_ATTRIBUTE, FIND_BY_ATTRIBUTE, FIND_BY_GROUP_NAME_APPROXIMATE, FIND_BY_TYPE, AND, OR, MINUS

        $this->setCommand('WsRestFindGroupsRequest');

        $params = [
            'wsQueryFilter' => [
                'groupName'       => $group,
                'stemName'        => $stem,
                'queryFilterType' => 'FIND_BY_GROUP_NAME_APPROXIMATE'
            ]
        ];

        $this->setParams($params);
        $this->setBody();

        // dd($this->body);

        return $this->apiCall('post', 'groups');
    }

    /**
     * hasMemberLite - a hasMemberLite request
     * https://spaces.internet2.edu/display/Grouper/Has+Member
     *
     * @param mixed $stem
     * @param mixed $group
     * @param mixed $subjectId
     *
     * @return void
     */
    public function hasMemberLite($stem, $group, $subjectId)
    {
        return $this->apiCall('get', "groups/$stem:$group/members/$subjectId");
    }

    /**
     * hasMember - a hasMember request
     * https://spaces.internet2.edu/display/Grouper/Has+Member
     *
     * @param mixed $stem
     * @param mixed $group
     * @param mixed $subjectIdArray
     *
     * @return void
     */
    public function hasMember($stem, $group, $subjectIdArray)
    {
        $this->setCommand('WsRestHasMemberRequest');

        $lookups = $this->getLookupsArray($subjectIdArray, 'subjectId');

        $this->setParams(['subjectLookups' => $lookups]);
        $this->setBody();

        return $this->apiCall('post', "groups/$stem:$group/members");
    }
}
