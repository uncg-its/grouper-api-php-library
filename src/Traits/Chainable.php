<?php

namespace Uncgits\GrouperApi\Traits;

use Uncgits\GrouperApi\Exceptions\SubjectNotFoundException;

/**
 * Trait Chainable
 * @package Uncgits\GrouperApi\Traits
 */
trait Chainable
{
    /*
    |--------------------------------------------------------------------------
    | Chain Methods
    |--------------------------------------------------------------------------
    |
    | Helper Methods that will enable chained calls
    |
     */

    /**
     * @param $userId
     * @param null $fieldToSearch
     *
     * @return $this
     */
    public function asUser($userId, $fieldToSearch = null)
    {
        // Possible source ids: 'g:isa', 'g:gsa', 'uncg-person', 'grouperEntities', 'grouperExternal', 'jdbc'

        $userId = $this->mapSubjects($userId);

        $param = ['subjectId' => $userId];
        if (!is_null($fieldToSearch)) {
            $param['subjectIdentifier'] = $fieldToSearch;
        }

        $this->api->setParams(['actAsSubjectLookup' => $param]);

        return $this;
    }

    /**
     * @param $maxResults
     * @return $this
     */
    public function capResultsAt($maxResults)
    {
        $this->maxResults = $maxResults;
        return $this;
    }

    /**
     * @return $this
     */
    public function withGroupDetail()
    {
        $this->api->setParams(['includeGroupDetail' => 'T']);
        return $this;
    }

    /**
     * @return $this
     */
    public function withSubjectDetail()
    {
        $this->api->setParams(['includeSubjectDetail' => 'T']);
        return $this;
    }

    /**
     * @param $filterType
     *
     * @return $this
     */
    public function withMemberFilter($filterType)
    {
        $this->api->setParams(['memberFilter' => $filterType]);
        return $this;
    }

    /**
     * @param array $names
     *
     * @return $this
     */
    public function withSubjectAttributeNames(array $names = [])
    {
        if (count($names) == 0) {
            $names = $this->subjectAttributeNames;
        }

        $this->api->setParams(['subjectAttributeNames' => $names]);
        return $this;
    }

    /**
     * @param array $names
     *
     * @return $this
     */
    public function withSubjectAttributeNamesString(array $names = [])
    {
        if (count($names) == 0) {
            $names = $this->subjectAttributeNames;
        }

        $this->api->setParams(['subjectAttributeNames' => implode(',', $names)]);
        return $this;
    }

    /**
     * @param null $pageSize
     *
     * @return $this
     */
    public function withPagination()
    {
        $this->api->setParams([
            'pageSize'   => $this->pageSize,
            'pageNumber' => $this->pageNumber,
        ]);

        return $this;
    }

    /**
     * @param string $searchField
     *
     * @return $this
     */
    public function onField(string $searchField)
    {
        $this->setSubjectSearchField($searchField);
        return $this;
    }

    /**
     * @param array $sourceIds
     *
     * @return $this
     */
    public function withSourceIds(array $sourceIds)
    {
        $this->api->setParams(['sourceIds' => $sourceIds]);
        return $this;
    }

    /**
     * @param array $subjects
     *
     * @return $this
     * @throws SubjectNotFoundException
     */
    public function prefetchSubjectIds(array $subjects)
    {
        foreach ($subjects as $subject) {
            try {
                $lookedUpSubject = $this->searchSubject($subject, $this->subjectSearchField);
                $normalizedSubjectName = strtolower($subject); // normalize to lowercase

                $this->prefetchedSubjectsMap[$normalizedSubjectName] = $lookedUpSubject->id;
                $this->prefetchedSubjectsMapback[$lookedUpSubject->id] = $normalizedSubjectName; // to allow us to return back to the user in same format.
            } catch (SubjectNotFoundException $e) {
                if (!$this->continueOnError) {
                    throw $e;
                }
            }
        }

        $this->reset(); // reset the call properties so we're ready for the next.
        return $this;
    }

    /**
     * @param array $groups
     * @param bool $continueOnError
     *
     * @return $this
     * @throws SubjectNotFoundException
     */
    public function prefetchGroupIds(array $groups)
    {
        try {
            foreach ($groups as $group) {
                $lookedUpGroup = $this->searchGroup($group);

                $this->prefetchedGroupsMap[$group] = $lookedUpGroup->uuid;
                $this->prefetchedGroupsMapback[$lookedUpGroup->uuid] = $group;
            }

            $this->reset(); // reset the call properties so we're ready for the next.
            return $this;
        } catch (GroupNotFoundException $e) {
            if (!$this->continueOnError) {
                throw $e;
            }
        }
    }

    /**
     * @return $this
     */
    public function continueOnError()
    {
        $this->continueOnError = true;
        return $this;
    }
}
