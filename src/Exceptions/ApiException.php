<?php

namespace Uncgits\GrouperApi\Exceptions;

/**
 * Class ApiException
 * @package Uncgits\GrouperApi
 */
class ApiException extends \Exception
{

    /**
     * @var
     */
    protected $callStack;

    /**
     * @return mixed
     */
    public function getCallStack()
    {
        return $this->callStack;
    }

    /**
     * @param $callStack
     *
     * @return $this
     */
    public function withCallStack($callStack)
    {
        $this->callStack = $callStack;
        return $this;
    }
}
