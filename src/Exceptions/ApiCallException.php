<?php

namespace Uncgits\GrouperApi\Exceptions;

use Uncgits\GrouperApi\Exceptions\ApiException as BaseException;

class ApiCallException extends BaseException
{
}
