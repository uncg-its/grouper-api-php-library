<?php

namespace Uncgits\GrouperApi\Exceptions;

use Uncgits\GrouperApi\Exceptions\ApiCallException as BaseException;

class MethodNotFoundException extends BaseException
{
}
