<?php

namespace Uncgits\GrouperApi\Exceptions;

use Uncgits\GrouperApi\Exceptions\EmptyQueryResultException as BaseException;

class MembershipNotFoundException extends BaseException
{
}
