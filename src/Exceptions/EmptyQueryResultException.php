<?php

namespace Uncgits\GrouperApi\Exceptions;

use Uncgits\GrouperApi\Exceptions\ApiResultException as BaseException;

class EmptyQueryResultException extends BaseException
{
}
