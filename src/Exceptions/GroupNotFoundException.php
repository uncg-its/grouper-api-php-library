<?php

namespace Uncgits\GrouperApi\Exceptions;

use Uncgits\GrouperApi\Exceptions\EmptyQueryResultException as BaseException;

class GroupNotFoundException extends BaseException
{
}
