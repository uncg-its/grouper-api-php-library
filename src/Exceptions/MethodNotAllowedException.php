<?php

namespace Uncgits\GrouperApi\Exceptions;

use Uncgits\GrouperApi\Exceptions\ApiCallException as BaseException;

class MethodNotAllowedException extends BaseException
{
}
